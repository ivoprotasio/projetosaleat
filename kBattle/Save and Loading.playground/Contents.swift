//: A Cocoa based Playground to present user interface

import AppKit
import PlaygroundSupport

let nibFile = NSNib.Name(rawValue:"MyView")
var topLevelObjects : NSArray?

Bundle.main.loadNibNamed(nibFile, owner:nil, topLevelObjects: &topLevelObjects)
let views = (topLevelObjects as! Array<Any>).filter { $0 is NSView }

// Present the view in Playground
PlaygroundPage.current.liveView = views[0] as! NSView

let fileName = "Test"
let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)

let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt")

let text = "my text for my text file"
do {
    try text.write(to: fileURL, atomically: true, encoding: .utf8)
} catch {
    print("failed with error: \(error)")
}

do {
    let text2 = try String(contentsOf: fileURL, encoding: .utf8)
    print("Read back text: \(text2)")
}
catch {
    print("failed with error: \(error)")
}
