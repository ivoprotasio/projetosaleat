//
//  PokéClass.swift
//  PokéBattle
//
//Fontes:
//PokeList - https://docs.google.com/spreadsheets/d/1DYnYR1bu1oRQ5ZMNwl5emfYzoYkr6P-JvULtYUTGtSg/edit#gid=0
//Pokemon Base xp - https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_effort_value_yield
//MoveList - https://bulbapedia.bulbagarden.net/wiki/List_of_moves
//Status Formula - https://bulbapedia.bulbagarden.net/wiki/Statistic
//Link beteew ATK Status and Move Power: https://www.serebii.net/games/damage.shtml

import Foundation

//------Variáveis
var genero:String?
var nome:String?
var escolha:String?
var opcao:String?
var escolhas:String?
var opcaoAtaque:String?
var change:String?
//Player
var escolhido:[Pokemon] = [Pokemon(nome: "", hp: 0, atk: 0, def: 0, spa: 0,spd: 0,spe: 0, base_xp: 0, type: "")]
var current_hp:Int = 0
var current_atk:Int = 0
var current_def:Int = 0
var current_spa:Int = 0
var current_spd:Int = 0
var current_spe:Int = 0
var current_lvl:Int = 0
var current_xp:Int = 0

var reset_hp:Int = 0

var current_damage = 0

var movePokémon:[Move] = [Move(nome: "", pp: 0, power: 0, type: "")]
var n1:Int = 0
var n2:Int = 0
var n3:Int = 0
var n4:Int = 0
//NPC
var escolhidonpc:[Pokemon] = [Pokemon(nome: "", hp: 0, atk: 0, def: 0, spa: 0,spd: 0,spe: 0, base_xp: 0, type: "")]
var current_hpnpc:Int = 0
var current_atknpc:Int = 0
var current_defnpc:Int = 0
var current_spanpc:Int = 0
var current_spdnpc:Int = 0
var current_spenpc:Int = 0
var current_lvlnpc:Int = 0

var movePokémonNPC:[Move] = [Move(nome: "", pp: 0, power: 0, type: "")]

var nnpc1:Int = 0
var nnpc2:Int = 0
var nnpc3:Int = 0
var nnpc4:Int = 0

var r:Int = 0
var d:Int = 0
var l:Int = 7
var lvlcap:Int = 100
//------Fim das Variáveis

//------Pokémon Array
var pokémon:[Pokemon] = [Pokemon(nome: "Turtwig", hp: 55, atk: 68, def: 64, spa: 45,spd: 55,spe: 31, base_xp: 64, type: "grass"),
                          Pokemon(nome: "Grotle", hp: 75, atk: 89, def: 85, spa: 55,spd: 65, spe: 36, base_xp: 142, type: "grass"),
                          Pokemon(nome: "Torterra", hp: 95, atk: 109, def: 105, spa: 75,spd: 85, spe: 56, base_xp: 236, type: "grass"),
                          Pokemon(nome: "Chimchar", hp: 44, atk: 58, def: 44, spa: 58,spd: 44, spe: 61, base_xp: 62, type: "fire"),
                          Pokemon(nome: "Monferno", hp: 64, atk: 78, def: 52, spa: 78,spd: 52, spe: 81, base_xp: 142, type: "fire"),
                          Pokemon(nome: "Infernape", hp: 76, atk: 104, def: 71, spa: 104,spd: 71, spe: 108, base_xp: 240, type: "grass"),
                          Pokemon(nome: "Piplup", hp: 53, atk: 51, def: 53, spa: 61, spd: 56, spe: 40, base_xp: 63, type: "water"),
                          Pokemon(nome: "Prinplup", hp: 64, atk: 66, def: 68, spa: 81, spd: 76, spe: 50, base_xp: 142, type: "water"),
                          Pokemon(nome: "Empoleon", hp: 84, atk: 86, def: 88, spa: 111, spd: 101, spe: 60, base_xp: 239, type: "water"),
                          Pokemon(nome: "Starly", hp: 40, atk: 55, def: 30, spa: 30, spd: 30, spe: 60, base_xp: 49, type: "flying"),
                          Pokemon(nome: "Staravia", hp: 55, atk: 75, def: 50, spa: 40, spd: 40, spe: 80, base_xp: 119, type: "flying"),
                          Pokemon(nome: "Staraptor", hp: 85, atk: 120, def: 70, spa: 50, spd: 50, spe: 100, base_xp: 218, type: "flying"),
                          Pokemon(nome: "Bidoof", hp: 59, atk: 45, def: 40, spa: 35, spd: 40, spe: 31, base_xp: 50, type: "normal"),
                          Pokemon(nome: "Bibarel", hp: 79, atk: 85, def: 60, spa: 60, spd: 71, spe: 55, base_xp: 144, type: "water"),
                          Pokemon(nome: "Kricketot", hp: 77, atk: 85, def: 51, spa: 55, spd: 51, spe: 65, base_xp: 39, type: "bug"),
                          Pokemon(nome: "Kricketune", hp: 77, atk: 85, def: 51, spa: 55, spd: 51, spe: 65, base_xp: 134, type: "bug"),
                          Pokemon(nome: "Shinx", hp: 45, atk: 65, def: 34, spa: 40, spd: 34, spe: 45, base_xp: 53, type: "electric"),
                          Pokemon(nome: "Luxio", hp: 60, atk: 85, def: 49, spa: 60, spd: 49, spe: 60, base_xp: 127, type: "electric"),
                          Pokemon(nome: "Luxray", hp: 80, atk: 120, def: 79, spa: 95, spd: 79, spe: 70, base_xp: 235, type: "electric"),
                          Pokemon(nome: "Budew", hp: 40, atk: 30, def: 35, spa: 50, spd: 70, spe: 25, base_xp: 56, type: "grass"),
                          Pokemon(nome: "Roserade", hp: 60, atk: 70, def: 55, spa: 125, spd: 105, spe: 90, base_xp: 235, type: "grass"),
                          Pokemon(nome: "Carnidos", hp: 67, atk: 125, def: 40, spa: 30, spd: 30, spe: 58, base_xp: 70, type: "rock"),
                          Pokemon(nome: "Rampardos", hp: 97, atk: 165, def: 60, spa: 65, spd: 50, spe: 58, base_xp: 173, type: "rock"),
                          Pokemon(nome: "Shieldon", hp: 30, atk: 42, def: 118, spa: 42, spd: 88, spe: 30, base_xp: 70, type: "rock"),
                          Pokemon(nome: "Bastiodon", hp: 60, atk: 52, def: 168, spa: 47, spd: 138, spe: 30, base_xp: 173, type: "steel"),
                          Pokemon(nome: "Burmy", hp: 40, atk: 29, def: 45, spa: 29, spd: 45, spe: 36, base_xp: 45, type: "bug"),
                          Pokemon(nome: "Wormadam(P)", hp: 60, atk: 59, def: 85, spa: 79, spd: 105, spe: 35, base_xp: 148, type: "grass"),
                          Pokemon(nome: "Mothim", hp: 70, atk: 94, def: 50, spa: 94, spd: 50, spe: 66, base_xp: 148, type: "flying"),
                          Pokemon(nome: "Combee", hp: 30, atk: 30, def: 42, spa: 30, spd: 42, spe: 70, base_xp: 49, type: "bug"),
                          Pokemon(nome: "Vespiquen", hp: 77, atk: 80, def: 102, spa: 80, spd: 102, spe: 40, base_xp:166, type: "bug"),
                          Pokemon(nome: "Pachirisu", hp: 60, atk: 45, def: 70, spa: 45, spd: 90, spe: 95, base_xp: 142, type: "electric"),
                          Pokemon(nome: "Buizel", hp: 55, atk: 65, def: 35, spa: 60, spd: 30, spe: 85, base_xp: 66, type: "water"),
                          Pokemon(nome: "Floatzel", hp: 85, atk: 105, def: 55, spa: 85, spd: 50, spe: 115, base_xp: 173, type: "water"),
                          Pokemon(nome: "Cherubi", hp: 45, atk: 35, def: 45, spa: 62, spd: 53, spe: 35, base_xp: 55, type: "grass"),
                          Pokemon(nome: "Cherrim", hp: 70, atk: 60, def: 70, spa: 87, spd: 78, spe: 85, base_xp: 158, type: "grass"),
                          Pokemon(nome: "Shellos", hp: 76, atk: 48, def: 48, spa: 57, spd: 62, spe: 34, base_xp: 65, type: "water"),
                          Pokemon(nome: "Gastrodon", hp: 111, atk: 83, def: 68, spa: 92, spd: 82, spe: 39, base_xp: 166, type: "water"),
                          Pokemon(nome: "Ambipom", hp: 75, atk: 85, def: 51, spa: 55, spd: 51, spe: 65, base_xp: 169, type: "bug"),
                          Pokemon(nome: "Drifloon", hp: 90, atk: 50, def: 34, spa: 60, spd: 44, spe: 70, base_xp: 70, type: "ghost"),
                          Pokemon(nome: "Drifblim", hp: 150, atk: 80, def: 44, spa: 90, spd: 54, spe: 80, base_xp: 174, type: "ghost"),
                          Pokemon(nome: "Buneary", hp: 55, atk: 66, def: 44, spa: 44, spd: 56, spe: 85, base_xp: 70, type: "normal"),
                          Pokemon(nome: "Lopunny", hp: 65, atk: 76, def: 84, spa: 54, spd: 96, spe: 105, base_xp: 168, type: "normal"),
                          Pokemon(nome: "Mismagius", hp: 60, atk: 60, def: 60, spa: 105, spd: 105, spe: 105, base_xp: 173, type: "ghost"),
                          Pokemon(nome: "Honchkrow", hp: 100, atk: 125, def: 52, spa: 105, spd: 52, spe: 71, base_xp: 177, type: "flying"),
                          Pokemon(nome: "Glameow", hp: 49, atk: 55, def: 42, spa: 42, spd: 37, spe: 85, base_xp: 62, type: "dark"),
                          Pokemon(nome: "Purugly", hp: 71, atk: 82, def: 64, spa: 64, spd: 59, spe: 112, base_xp: 158, type: "dark"),
                          Pokemon(nome: "Chingling", hp: 45, atk: 30, def: 50, spa: 65, spd: 50, spe: 45, base_xp: 57, type: "Psychic")]

//------Moves Array
var move:[Move] = [Move(nome: "Pound", pp: 35, power: 40, type: "normal"),
                   Move(nome: "Karate Chop", pp: 25, power: 50, type: "fighting"),
                   Move(nome: "Double Slap", pp: 10, power: 15, type: "normal"),
                   Move(nome: "Comet Punch", pp: 15, power: 18, type: "normal"),
                   Move(nome: "Mega Punch", pp: 20, power: 80, type: "normal"),
                   Move(nome: "Pay Day", pp: 20, power: 40, type: "normal"),
                   Move(nome: "Fire Punch", pp: 15, power: 75, type: "fire"),
                   Move(nome: "Ice Punch", pp: 15, power: 75, type: "ice"),
                   Move(nome: "Thunder Punch", pp: 15, power: 75, type: "electric"),
                   Move(nome: "Scratch", pp: 35, power: 40, type: "normal"),
                   Move(nome: "Vice Grip", pp: 30, power: 55, type: "normal"),
                   Move(nome: "Razor Wind", pp: 10, power: 80, type: "normal"),
                   Move(nome: "Cut", pp: 30, power: 50, type: "normal"),
                   Move(nome: "Gust", pp: 35, power: 40, type: "flying"),
                   Move(nome: "Wing Attack", pp: 35, power: 60, type: "flying"),
                   Move(nome: "Fly", pp: 20, power: 90, type: "flying"),
                   Move(nome: "Bind", pp: 20, power: 15, type: "normal"),
                   Move(nome: "Slam", pp: 20, power: 80, type: "normal"),
                   Move(nome: "Vine Whip", pp: 25, power: 45, type: "grass"),
                   Move(nome: "Stomp", pp: 20, power: 65, type: "normal"),
                   Move(nome: "Double Kick", pp: 30, power: 30, type: "fighting"),
                   Move(nome: "Mega Kick", pp: 5, power: 120, type: "normal"),
                   Move(nome: "Jump Kick", pp: 10, power: 100, type: "fighting"),
                   Move(nome: "Rolling Kick", pp: 15, power: 60, type: "fighting"),
                   Move(nome: "Headbutt", pp: 15, power: 70, type: "normal"),
                   Move(nome: "Horn Attack", pp: 25, power: 65, type: "normal"),
                   Move(nome: "Fury Attack", pp: 20, power: 15, type: "normal"),
                   Move(nome: "Tackle", pp: 35, power: 40, type: "normal"),
                   Move(nome: "Body Slam", pp: 15, power: 85, type: "normal"),
                   Move(nome: "Wrap", pp: 20, power: 15, type: "normal"),
                   Move(nome: "Take Down", pp: 20, power: 90, type: "normal"),
                   Move(nome: "Thrash", pp: 10, power: 120, type: "normal"),
                   Move(nome: "Double-Edge", pp: 15, power: 120, type: "normal"),
                   Move(nome: "Poison Sting", pp: 35, power: 15, type: "poison"),
                   Move(nome: "Twineedle", pp: 20, power: 25, type: "bug"),
                   Move(nome: "Pin Missile", pp: 20, power: 25, type: "bug"),
                   Move(nome: "Bite", pp: 25, power: 60, type: "dark"),
                   Move(nome: "Acid", pp: 30, power: 40, type: "poison"),
                   Move(nome: "Ember", pp: 25, power: 40, type: "fire"),
                   Move(nome: "Flamethrower", pp: 15, power: 90, type: "fire"),
                   Move(nome: "Water Gun", pp: 25, power: 40, type: "water"),
                   Move(nome: "Hydro Pump", pp: 5, power: 110, type: "water"),
                   Move(nome: "Surf", pp: 15, power: 90, type: "water"),
                   Move(nome: "Ice Beam", pp: 10, power: 90, type: "ice"),
                   Move(nome: "Blizzard", pp: 5, power: 110, type: "ice"),
                   Move(nome: "Psybeam", pp: 20, power: 65, type: "psychic"),
                   Move(nome: "Bubble Beam", pp: 20, power: 65, type: "water"),
                   Move(nome: "Aurora Beam", pp: 20, power: 65, type: "ice"),
                   Move(nome: "Hyper Beam", pp: 5, power: 150, type: "normal"),
                   Move(nome: "Peck", pp: 35, power: 35, type: "flying"),
                   Move(nome: "Drill Peck", pp: 20, power: 80, type: "flying"),
                   Move(nome: "Submission", pp: 20, power: 80, type: "fighting"),
                   Move(nome: "Strength", pp: 15, power: 80, type: "normal"),
                   Move(nome: "Absorb", pp: 25, power: 20, type: "grass"),
                   Move(nome: "Mega Drain", pp: 15, power: 40, type: "grass"),
                   Move(nome: "Razor Leaf", pp: 25, power: 55, type: "grass"),
                   Move(nome: "Solar Beam", pp: 10, power: 120, type: "grass"),
                   Move(nome: "Petal Dance", pp: 10, power: 120, type: "grass"),
                   Move(nome: "Fire Spin", pp: 15, power: 35, type: "fire"),
                   Move(nome: "Thunder Shock", pp: 30, power: 40, type: "electric"),
                   Move(nome: "Thunderbolt", pp: 15, power: 90, type: "electric"),
                   Move(nome: "Thunder", pp: 10, power: 110, type: "electric"),
                   Move(nome: "Rock Throw", pp: 15, power: 50, type: "rock"),
                   Move(nome: "Earthquake", pp: 10, power: 100, type: "ground"),
                   Move(nome: "Dig", pp: 10, power: 80, type: "ground"),
                   Move(nome: "Confusion", pp: 25, power: 50, type: "psychic"),
                   Move(nome: "Psychic", pp: 10, power: 90, type: "psychic"),
                   Move(nome: "Quick Attack", pp: 30, power: 40, type: "normal"),
                   Move(nome: "Rage", pp: 20, power: 20, type: "normal"),
                   Move(nome: "Self-Destruct", pp: 5, power: 200, type: "normal"),
                   Move(nome: "Egg Bomb", pp: 10, power: 100, type: "normal"),
                   Move(nome: "Lick", pp: 30, power: 30, type: "ghost"),
                   Move(nome: "Smog", pp: 20, power: 30, type: "poison"),
                   Move(nome: "Sludge", pp: 20, power: 65, type: "poison"),
                   Move(nome: "Bone Club", pp: 20, power: 65, type: "ground"),
                   Move(nome: "Fire Blast", pp: 5, power: 110, type: "fire"),
                   Move(nome: "Waterfall", pp: 15, power: 80, type: "water"),
                   Move(nome: "Clamp", pp: 15, power: 35, type: "water"),
                   Move(nome: "Swift", pp: 20, power: 60, type: "normal"),
                   Move(nome: "Skull Bash", pp: 10, power: 130, type: "normal"),
                   Move(nome: "Spike Cannon", pp: 15, power: 20, type: "normal"),
                   Move(nome: "Constrict", pp: 35, power: 10, type: "normal"),
                   Move(nome: "Bubble", pp: 30, power: 40, type: "water")]

//------Potions
var potion:[Potion] = [Potion(hp: 20),
                       Potion(hp: 20),
                       Potion(hp: 20),
                       Potion(hp: 20),
                       Potion(hp: 20),
                       Potion(hp: 20),
                       Potion(hp: 20),
                       Potion(hp: 20),
                       Potion(hp: 20),
                       Potion(hp: 20),]
//Funções
func Damage() -> Int {
    let calc:Int = (((2*current_lvl)/5+2)*current_atk*movePokémon[0].power/current_def)/50+2
    return calc
}
func DamageRandom() -> Int {
    d = Int(arc4random_uniform(3))
    return d
}
DamageRandom()
func DamageNPC() -> Int {
    let calc:Int = (((2*current_lvl)/5+2)*current_atk*movePokémonNPC[d].power/current_def)/50+2
    return calc
}
func changeMove(){
    let a:Int = Int(arc4random_uniform(82))
    if(current_lvl == l){
        print("Your Pokémon is about to recive a new move called: \(move[a].nome)\n")
        print("Choose a move to discard:")
        print("1 - \(movePokémon[0].nome)")
        print("2 - \(movePokémon[1].nome)")
        print("3 - \(movePokémon[2].nome)")
        print("4 - \(movePokémon[3].nome)\n")
        change = readLine()
        switch(change!){
        case "1":
            movePokémon.remove(at: 0)
            movePokémon.append(move[a])
            l = l + 5
            break
        case "2":
            movePokémon.remove(at: 1)
            movePokémon.append(move[a])
            l = l + 5
            break
        case "3":
            movePokémon.remove(at: 2)
            movePokémon.append(move[a])
            l = l + 5
            break
        case "4":
            movePokémon.remove(at: 3)
            movePokémon.append(move[a])
            l = l + 5
            break
        default:
            break
        }
    }
}

//Random
func randomMove1() -> Int{
    n1 = Int(arc4random_uniform(82))
    return n1
}

func randomMove2() -> Int{
    n2 = Int(arc4random_uniform(82))
    return n2
}

func randomMove3() -> Int{
    n3 = Int(arc4random_uniform(82))
    return n3
}

func randomMove4() -> Int{
    n4 = Int(arc4random_uniform(82))
    return n4
}
//NPC Pokémon
func randomPokémon() -> Int{
    r = Int(arc4random_uniform(46))
    return r
}
func randomMoveNPC1() -> Int{
    nnpc1 = Int(arc4random_uniform(82))
    return nnpc1
}
func randomMoveNPC2() -> Int{
    nnpc2 = Int(arc4random_uniform(82))
    return nnpc2
}
func randomMoveNPC3() -> Int{
    nnpc3 = Int(arc4random_uniform(82))
    return nnpc3
}
func randomMoveNPC4() -> Int{
    nnpc4 = Int(arc4random_uniform(82))
    return nnpc4
}
//Fim Funções

//Inicio
print("Welcome to the PokéBattle Simulator v2.0\n")
//------Escolha do genero
repeat {
    //print(move.count)
    //print(pokémon.count)
    print("Choose your gender 1 - Male or 2 - Female")
    genero = readLine()
    
} while genero != "1" && genero != "2"

//------Escolha do nome
repeat {
    print("What's your Treiner name?")
    nome = readLine()
    
} while nome == ""

//------Escolha do pokémon
repeat {
    print("Choose your first Pokémon: 1 - Turtwig / 2 - Chimchar / 3 - Piplup")
    escolha = readLine()
    
} while escolha != "1" && escolha != "2" && escolha != "3"

switch (escolha!){
case "1":
    escolhido = [pokémon[0]]
    print("The choosen Pokemon was: \(escolhido[0].nome)\n")
    print("Pokemon Status")
    current_lvl = escolhido[0].lvl
    print("Level: \(current_lvl)")
    current_hp = escolhido[0].calcHPStatus()
    print("HP Status: \(current_hp)")
    current_atk = escolhido[0].calcATKStatus()
    print("ATK Status: \(current_atk)")
    current_def = escolhido[0].calcDEFStatus()
    print("DEF Status: \(current_def)")
    current_spa = escolhido[0].calcSPAStatus()
    print("SPA Status: \(current_spa)")
    current_spd = escolhido[0].calcSPDStatus()
    print("SPD Status: \(current_spd)")
    current_spe = escolhido[0].calcSPEStatus()
    print("SPE Status: \(current_spe)\n")
    
    randomMove1()
    randomMove2()
    randomMove3()
    randomMove4()
    movePokémon = [move[n1],
                    move[n2],
                    move[n3],
                    move[n4]]
    print("Move:")
    print("1 - \(movePokémon[0].nome): \(movePokémon[0].power) Power")
    print("2 - \(movePokémon[1].nome): \(movePokémon[1].power) Power")
    print("3 - \(movePokémon[2].nome): \(movePokémon[2].power) Power")
    print("4 - \(movePokémon[3].nome): \(movePokémon[3].power) Power\n")
    break
case "2":
    escolhido = [pokémon[3]]
    print("The choosen Pokemon was: \(escolhido[0].nome)\n")
    print("Pokemon Status")
    current_lvl = escolhido[0].lvl
    print("Level: \(current_lvl)")
    current_hp = escolhido[0].calcHPStatus()
    print("HP Status: \(current_hp)")
    current_atk = escolhido[0].calcATKStatus()
    print("ATK Status: \(current_atk)")
    current_def = escolhido[0].calcDEFStatus()
    print("DEF Status: \(current_def)")
    current_spa = escolhido[0].calcSPAStatus()
    print("SPA Status: \(current_spa)")
    current_spd = escolhido[0].calcSPDStatus()
    print("SPD Status: \(current_spd)")
    current_spe = escolhido[0].calcSPEStatus()
    print("SPE Status: \(current_spe)\n")
    
    randomMove1()
    randomMove2()
    randomMove3()
    randomMove4()
    movePokémon = [move[n1],
                    move[n2],
                    move[n3],
                    move[n4]]
    print("Move:")
    print("1 - \(movePokémon[0].nome): \(movePokémon[0].power) Power")
    print("2 - \(movePokémon[1].nome): \(movePokémon[1].power) Power")
    print("3 - \(movePokémon[2].nome): \(movePokémon[2].power) Power")
    print("4 - \(movePokémon[3].nome): \(movePokémon[3].power) Power\n")
    break
case "3":
    escolhido = [pokémon[6]]
    print("The choosen Pokemon was: \(escolhido[0].nome)\n")
    print("Pokemon Status")
    current_lvl = escolhido[0].lvl
    print("Level: \(current_lvl)")
    current_hp = escolhido[0].calcHPStatus()
    print("HP Status: \(current_hp)")
    current_atk = escolhido[0].calcATKStatus()
    print("ATK Status: \(current_atk)")
    current_def = escolhido[0].calcDEFStatus()
    print("DEF Status: \(current_def)")
    current_spa = escolhido[0].calcSPAStatus()
    print("SPA Status: \(current_spa)")
    current_spd = escolhido[0].calcSPDStatus()
    print("SPD Status: \(current_spd)")
    current_spe = escolhido[0].calcSPEStatus()
    print("SPE Status: \(current_spe)\n")
    
    randomMove1()
    randomMove2()
    randomMove3()
    randomMove4()
    movePokémon = [move[n1],
                    move[n2],
                    move[n3],
                    move[n4]]
    print("Move:")
    print("1 - \(movePokémon[0].nome): \(movePokémon[0].power) Power")
    print("2 - \(movePokémon[1].nome): \(movePokémon[1].power) Power")
    print("3 - \(movePokémon[2].nome): \(movePokémon[2].power) Power")
    print("4 - \(movePokémon[3].nome): \(movePokémon[3].power) Power\n")
    break
    
default:
    break
}
//
print("Main Menu:")
repeat {
    repeat {
        print("1 - Search Battle")
        print("2 - Exit\n")
        escolha = readLine()
    } while escolha != "1" && escolha != "2"
    
    switch (escolha!){
    case "1":
        randomPokémon()
        randomMoveNPC1()
        randomMoveNPC2()
        randomMoveNPC3()
        randomMoveNPC4()
        //Gerar Pokémon Enemigo
        escolhidonpc = [pokémon[r]]
        movePokémonNPC = [move[nnpc1],
                           move[nnpc2],
                           move[nnpc3],
                           move[nnpc4],]
        current_lvlnpc = escolhidonpc[0].lvl
        current_hpnpc = escolhidonpc[0].calcHPStatus()
        current_atknpc = escolhidonpc[0].calcATKStatus()
        current_defnpc = escolhidonpc[0].calcDEFStatus()
        current_spanpc = escolhidonpc[0].calcSPAStatus()
        current_spdnpc = escolhidonpc[0].calcSPDStatus()
        current_spenpc = escolhidonpc[0].calcSPEStatus()
        
        current_hp = escolhido[0].calcHPStatus()
        current_atk = escolhido[0].calcATKStatus()
        current_def = escolhido[0].calcDEFStatus()
        current_spa = escolhido[0].calcSPAStatus()
        current_spd = escolhido[0].calcSPDStatus()
        current_spe = escolhido[0].calcSPEStatus()
        
        print("Match Found!!!")
        print("The NPC choose \(escolhidonpc[0].nome) to battle.\n")
        break
    case "2":
        current_hp = 0
        break
    default:
        break
    }
    
    repeat{
        switch (escolha!){
        case "1":
            //Menu da Batalha
            repeat {
                print("Battle Menu:")
                print("1 - Attack")
                print("2 - Bag")
                print("3 - Run\n")
                opcao = readLine()
            } while opcao != "1" && opcao != "2" && opcao != "3"
            //Interassao para a batalha
            switch (opcao!){
            case "1":
                print("Select a Move:")
                print("1 - \(movePokémon[0].nome): \(movePokémon[0].power) Power")
                print("2 - \(movePokémon[1].nome): \(movePokémon[1].power) Power")
                print("3 - \(movePokémon[2].nome): \(movePokémon[2].power) Power")
                print("4 - \(movePokémon[3].nome): \(movePokémon[3].power) Power\n")
                opcaoAtaque = readLine()
                if(opcaoAtaque == "1" ){
                    current_hpnpc = current_hpnpc - Damage()
                    print("\(escolhido[0].nome) use \(movePokémon[0].nome)!\n")
                    if(current_hpnpc > 0){
                        current_hp = current_hp - DamageNPC()
                        print("\(escolhidonpc[0].nome) used \(movePokémonNPC[d].nome)!\n")
                    }else if(current_hpnpc <= 0){
                        print("\(escolhidonpc[0].nome) fainted.\n")
                        print("You won the battle!\n")
                    }
                    if(current_hpnpc > 0){
                        print("Your Pokémon HP: \(current_hp)")
                        print("Opponent Pókemon HP: \(current_hpnpc)\n")
                    }
                } else if(opcaoAtaque == "2"){
                    current_hpnpc = current_hpnpc - Damage()
                    print("\(escolhido[0].nome) use \(movePokémon[1].nome)!\n")
                    if(current_hpnpc > 0){
                        current_hp = current_hp - DamageNPC()
                        print("\(escolhidonpc[0].nome) used \(movePokémonNPC[d].nome)!\n")
                    }else if(current_hpnpc <= 0){
                        print("\(escolhidonpc[0].nome) fainted.\n")
                        print("You won the battle!\n")
                    }
                    if(current_hpnpc > 0){
                        print("Your Pokémon HP: \(current_hp)")
                        print("Opponent Pókemon HP: \(current_hpnpc)\n")
                    }
                } else if(opcaoAtaque == "3"){
                    current_hpnpc = current_hpnpc - Damage()
                    print("\(escolhido[0].nome) use \(movePokémon[2].nome)!\n")
                    if(current_hpnpc > 0){
                        current_hp = current_hp - DamageNPC()
                        print("\(escolhidonpc[0].nome) used \(movePokémonNPC[d].nome)!\n")
                    }else if(current_hpnpc <= 0){
                        print("\(escolhidonpc[0].nome) fainted.\n")
                        print("You won the battle!\n")
                    }
                    if(current_hpnpc > 0){
                        print("Your Pokémon HP: \(current_hp)")
                        print("Opponent Pókemon HP: \(current_hpnpc)\n")
                    }
                } else if(opcaoAtaque == "4"){
                    current_hpnpc = current_hpnpc - Damage()
                    print("\(escolhido[0].nome) use \(movePokémon[3].nome)!\n")
                    if(current_hpnpc > 0){
                        current_hp = current_hp - DamageNPC()
                        print("\(escolhidonpc[0].nome) used \(movePokémonNPC[d].nome)!\n")
                    }else if(current_hpnpc <= 0){
                        print("\(escolhidonpc[0].nome) fainted.\n")
                        print("You won the battle!\n")
                    }
                    if(current_hpnpc > 0){
                        print("Your Pokémon HP: \(current_hp)")
                        print("Opponent Pókemon HP: \(current_hpnpc)\n")
                    }
                }
                
                if (current_hpnpc <= 0){
                    current_xp = escolhido[0].calcEXP() + current_xp
                    print("Experience recived: \(escolhido[0].calcEXP())\n")
                    if(current_xp >= lvlcap){
                        current_lvl = current_lvl + 1
                        if(escolhido[0].nome == pokémon[0].nome && current_lvl == 7){
                            escolhido.remove(at: 0)
                            escolhido.append(pokémon[1])
                            print("Your Pokémon is evolving into: \(escolhido[0].nome)\n")
                        }
                        if(escolhido[0].nome == pokémon[1].nome && current_lvl == 10){
                            escolhido.remove(at: 0)
                            escolhido.append(pokémon[2])
                            print("Your Pokémon is evolving into: \(escolhido[0].nome)\n")
                        }
                        if(escolhido[0].nome == pokémon[3].nome && current_lvl == 8){
                            escolhido.remove(at: 0)
                            escolhido.append(pokémon[4])
                            print("Your Pokémon is evolving into: \(escolhido[0].nome)\n")
                        }
                        if(escolhido[0].nome == pokémon[4].nome && current_lvl == 11){
                            escolhido.remove(at: 0)
                            escolhido.append(pokémon[5])
                            print("Your Pokémon is evolving into: \(escolhido[0].nome)\n")
                        }
                        if(escolhido[0].nome == pokémon[6].nome && current_lvl == 9){
                            escolhido.remove(at: 0)
                            escolhido.append(pokémon[7])
                            print("Your Pokémon is evolving into: \(escolhido[0].nome)\n")
                        }
                        if(escolhido[0].nome == pokémon[7].nome && current_lvl == 12){
                            escolhido.remove(at: 0)
                            escolhido.append(pokémon[8])
                            print("Your Pokémon is evolving into: \(escolhido[0].nome)\n")
                        }
                        current_xp = 0
                        print("You Pokémon Level up to level \(current_lvl)\n")
                        lvlcap = lvlcap + 50
                        changeMove()
                    }
                }
                break
            case "2":
                repeat{
                    print("1 - Potion x\(potion.count)")
                    print("2 - Back")
                    escolhas = readLine()
                    if(potion.count > 0){
                        if(escolhas == "1"){
                            if(current_damage <= 20){
                                current_hp = escolhido[0].calcHPStatus()
                                print("The Pokémon restore all hp.\n")
                                current_damage = 0
                                potion.remove(at: 0)
                            }else if(current_damage > 20){
                                current_hp = current_hp + potion[0].hp
                                current_damage = current_damage - potion[0].hp
                                print("The Pokémon restore 20 hp.\n")
                                potion.remove(at: 0)
                            }
                        }
                    }else{
                        print("You don´t have potions to use.\n")
                    }
                    
                } while escolhas != "1" && escolhas != "2"
                break
            case "3":
                current_hp = 0
                break
            default:
                break
            }
            break
        case "2":
            break
        default:
            break
        }
        
    } while current_hp > 0 && current_hpnpc > 0
} while current_hp > 0 || escolha != "2"

