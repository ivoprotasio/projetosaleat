//
//  MainMenu.swift
//  KBattle
//
//  Copyright © 2018 Andre&Ivo. All rights reserved.
//

import Foundation

import UIKit

class MainMenu: UIViewController {
    
    //Menu Buttons
    @IBOutlet weak var Battlebtn: UIButton!
    @IBOutlet weak var Pokedexbtn: UIButton!
    @IBOutlet weak var Inventorybtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Battle Button
        Battlebtn.setImage(UIImage(named: "Battle_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        Battlebtn.setImage(UIImage(named: "Battle_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
        //PokéDex Button
        Pokedexbtn.setImage(UIImage(named: "Pokédex_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        Pokedexbtn.setImage(UIImage(named: "Pokédex_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
        //Inventory Button
        Inventorybtn.setImage(UIImage(named: "Inventory_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        Inventorybtn.setImage(UIImage(named: "Inventory_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        
    }
    
}
