//
//  InventoryMenu.swift
//  KBattle
//
//  Copyright © 2018 Andre&Ivo. All rights reserved.
//

import Foundation

import UIKit

class InsideInventory: UIViewController {
    
    @IBOutlet weak var Back: UIButton!
    //Images
    @IBOutlet weak var Team01: UIImageView!
    @IBOutlet weak var Team02: UIImageView!
    @IBOutlet weak var Team03: UIImageView!
    @IBOutlet weak var Team04: UIImageView!
    @IBOutlet weak var Team05: UIImageView!
    @IBOutlet weak var Team06: UIImageView!
    //Labels
    @IBOutlet weak var NTeam01: UILabel!
    @IBOutlet weak var NTeam02: UILabel!
    @IBOutlet weak var NTeam03: UILabel!
    @IBOutlet weak var NTeam04: UILabel!
    @IBOutlet weak var NTeam05: UILabel!
    @IBOutlet weak var NTeam06: UILabel!
    
    let team = MyVariables.equipa
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Back Button
        Back.setImage(UIImage(named: "Back_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        Back.setImage(UIImage(named: "Bag_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
        //Team Load
        let pokecount:Int = Int(team.count)
        
        switch pokecount {
        case 1:
            self.Team01.image = UIImage(named: "\(team[0].indexnum)")
            self.NTeam01.text = "\(team[0].nome)"
            break
        case 2:
            self.Team01.image = UIImage(named: "\(team[0].indexnum)")
            self.NTeam01.text = "\(team[0].nome)"
            self.Team02.image = UIImage(named: "\(team[1].indexnum)")
            self.NTeam02.text = "\(team[1].nome)"
            break
        case 3:
            self.Team01.image = UIImage(named: "\(team[0].indexnum)")
            self.NTeam01.text = "\(team[0].nome)"
            self.Team02.image = UIImage(named: "\(team[1].indexnum)")
            self.NTeam02.text = "\(team[1].nome)"
            self.Team03.image = UIImage(named: "\(team[2].indexnum)")
            self.NTeam03.text = "\(team[2].nome)"
            break
        case 4:
            self.Team01.image = UIImage(named: "\(team[0].indexnum)")
            self.NTeam01.text = "\(team[0].nome)"
            self.Team02.image = UIImage(named: "\(team[1].indexnum)")
            self.NTeam02.text = "\(team[1].nome)"
            self.Team03.image = UIImage(named: "\(team[2].indexnum)")
            self.NTeam03.text = "\(team[2].nome)"
            self.Team04.image = UIImage(named: "\(team[3].indexnum)")
            self.NTeam04.text = "\(team[3].nome)"
            break
        case 5:
            self.Team01.image = UIImage(named: "\(team[0].indexnum)")
            self.NTeam01.text = "\(team[0].nome)"
            self.Team02.image = UIImage(named: "\(team[1].indexnum)")
            self.NTeam02.text = "\(team[1].nome)"
            self.Team03.image = UIImage(named: "\(team[2].indexnum)")
            self.NTeam03.text = "\(team[2].nome)"
            self.Team04.image = UIImage(named: "\(team[3].indexnum)")
            self.NTeam04.text = "\(team[3].nome)"
            self.Team05.image = UIImage(named: "\(team[4].indexnum)")
            self.NTeam05.text = "\(team[4].nome)"
            break
        case 6:
            self.Team01.image = UIImage(named: "\(team[0].indexnum)")
            self.NTeam01.text = "\(team[0].nome)"
            self.Team02.image = UIImage(named: "\(team[1].indexnum)")
            self.NTeam02.text = "\(team[1].nome)"
            self.Team03.image = UIImage(named: "\(team[2].indexnum)")
            self.NTeam03.text = "\(team[2].nome)"
            self.Team04.image = UIImage(named: "\(team[3].indexnum)")
            self.NTeam04.text = "\(team[3].nome)"
            self.Team05.image = UIImage(named: "\(team[4].indexnum)")
            self.NTeam05.text = "\(team[4].nome)"
            self.Team06.image = UIImage(named: "\(team[5].indexnum)")
            self.NTeam06.text = "\(team[5].nome)"
            break
        default:
            self.Team01.image = UIImage(named: "000")
            self.NTeam01.text = "???"
            self.Team02.image = UIImage(named: "000")
            self.NTeam02.text = "???"
            self.Team03.image = UIImage(named: "000")
            self.NTeam03.text = "???"
            self.Team04.image = UIImage(named: "000")
            self.NTeam04.text = "???"
            self.Team05.image = UIImage(named: "000")
            self.NTeam05.text = "???"
            self.Team06.image = UIImage(named: "000")
            self.NTeam06.text = "???"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
