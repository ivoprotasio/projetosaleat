//
//  Pokedex.swift
//  KBattle
//
//  Copyright © 2018 Andre&Ivo. All rights reserved.
//
//https://www.youtube.com/watch?v=tGr7qsKGkzY - pickerView

import Foundation
import UIKit

class PokeDex: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    //Choosen Pokémon Preview
    @IBOutlet weak var index_Preview: UIImageView!
    @IBOutlet weak var index_Number: UILabel!
    @IBOutlet weak var index_Type: UILabel!
    //Pokemon List
    @IBOutlet weak var PokeIndex: UIPickerView!
    //Buttons
    @IBOutlet weak var CreatePokemon: UIButton!
    @IBOutlet weak var Back: UIButton!
    var pokemon = MyVariables.pokedex
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        pokemon = MyVariables.pokedex
        PokeIndex.reloadAllComponents()
        pokemon.sort(by: {$1.indexnum > $0.indexnum})
        //Create Button
        CreatePokemon.setImage(UIImage(named: "Create_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        CreatePokemon.setImage(UIImage(named: "Create_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
        //Back Button
        Back.setImage(UIImage(named: "Back_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        Back.setImage(UIImage(named: "Back_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
        print(pokemon.count)
    }
    @IBAction func onClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //Fill with Pokemon Array Data
        return pokemon[row].nome
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //Match with the number of Existing Pokémons
        return pokemon.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(Int(pokemon[row].indexnum)! > 47){
            self.index_Preview.image = UIImage(named: "000")
        }else{
            self.index_Preview.image = UIImage(named: "\(pokemon[row].indexnum)")
        }
        
        self.index_Type.text = "\(pokemon[row].type)"
        self.index_Number.text = "#"+"\(pokemon[row].indexnum)"
        
    }
}

