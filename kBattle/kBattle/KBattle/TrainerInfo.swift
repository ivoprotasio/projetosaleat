//
//  TrainerInfo.swift
//  KBattle
//
//  Created by André Alexandre Ortiz Rijo on 23/06/2018.
//  Copyright © 2018 Andre&Ivo. All rights reserved.
//
//https://stackoverflow.com/questions/25223407/max-length-uitextfield

import Foundation

import UIKit

class Info: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var TrainerName: UITextField!
    @IBOutlet weak var Nextbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Nextbtn.setImage(UIImage(named: "Next_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        Nextbtn.setImage(UIImage(named: "Next_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
        
        TrainerName.delegate = self
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //Function to limit the number of characters
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard TrainerName.text != nil else { return true }
        let newLength = (TrainerName.text?.characters.count)! + string.characters.count - range.length
        return newLength <= 9
    }
    //Pass values between ViewControllers
    @IBAction func Next(_ sender: Any) {
        performSegue(withIdentifier: "Nome", sender: TrainerName.text!)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        
    }
    
}
