//
//  TheCreation.swift
//  KBattle
//
//  Copyright © 2018 Andre&Ivo. All rights reserved.
//

import Foundation

import UIKit

class Creation: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate  {
    
    //Poke Information
    @IBOutlet weak var NewPokeImage: UIImageView!
    @IBOutlet weak var NewPokeName: UITextField!
    @IBOutlet weak var NewPokeType: UIPickerView!
    
    //Buttons
    @IBOutlet weak var CreatePoke: UIButton!
    @IBOutlet weak var Back: UIButton!
    
    let oldpokemon = MyVariables.pokemon
    var tipo:String = "grass"
    let newtype = ["fire","grass","water","normal","ice","fighting","dark","psychic","flying","rock","ground","ghost","steel","electric","bug"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Button
        CreatePoke.setImage(UIImage(named: "Create_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        CreatePoke.setImage(UIImage(named: "Create_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
        Back.setImage(UIImage(named: "Back_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        Back.setImage(UIImage(named: "Bag_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //Fill with Pokemon Array Data
        return newtype[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //Match with the number of Existing Pokémons
        return newtype.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        tipo = newtype[row]
        
        //newpokemon.type = newtype[row]
        //newpokemon.indexnum = MyVariables.pokemon.last?.indexnum
        //MyVariables.pokemon.append(newpokemon)
        
    }
    
    @IBAction func onClose(_ sender: UIButton) {
        let i = sender.tag
        if(i == 0){
            dismiss(animated: true, completion: nil)
        }else if(i == 1){
            
            if(NewPokeName.text != ""){
                let x = MyVariables.pokemon.count + 1
                let newpokemon = Pokemon(nome: NewPokeName.text!, hp: 20, atk: 20, def: 20, spa: 20, spd: 20, spe: 20, base_xp: 0, type: tipo, trainer_v: "000_b", opponent_v: "000", indexnum: "0" + String(x))
                
                MyVariables.pokemon.append(newpokemon)
                MyVariables.pokedex.append(newpokemon)
            }
            dismiss(animated: true, completion: nil)
        }
    }
    
}

